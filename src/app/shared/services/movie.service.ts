import {Injectable} from '@angular/core';
import {environmentApi} from "../../../environment/environment.api";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {catchError, map, Observable, take, throwError} from "rxjs";
import {IMoviesData} from "../interfaces/movies-data.interface";
import {ITLMovieInfo} from "../interfaces/tl-movie-info.interface";

@Injectable({
  providedIn: 'root'
})
export class MovieService {
  constructor(
    private http: HttpClient
  ) {}

  public headers: HttpHeaders = new HttpHeaders({
    accept: 'application/json',
    Authorization: 'Bearer ' + environmentApi.API_READ_ACCESS_TOKEN
  });

  public getPopularMoviesByPage(pageNumber: number = 1, movieLanguage = 'en-US'): Observable<{
    status: number,
    data: IMoviesData
  }> {
    const headers = this.headers
    const url = `${environmentApi.BACKEND_URL}movie/popular?language=${movieLanguage}&page=${pageNumber}'`
    return this.http.get<IMoviesData>(url, {headers}).pipe(take(1)).pipe(map(response => Object.assign({
      status: 200,
      data: response
    })), catchError(err => throwError({status: err.status, error: err})))
  }

  public getMovieDetailsById(movieId: string, language = 'en-US'): Observable<{
    status: number,
    data: ITLMovieInfo
  }> {

    const headers = this.headers
    const url = `${environmentApi.BACKEND_URL}movie/${movieId}?language=${language}'`
    return this.http.get<ITLMovieInfo>(url, {headers}).pipe(take(1)).pipe(map(response => Object.assign({
      status: 200,
      data: response
    })), catchError(err => throwError({status: err.status, error: err})))

  }

  public findMovieByName(keyWord: string, language = 'en-US'): Observable<{
    status: number,
    data: IMoviesData
  }> {

    const headers = this.headers
    const url = `${environmentApi.BACKEND_URL}search/movie?query=${keyWord}&include_adult=true&language=${language}`
    return this.http.get<ITLMovieInfo>(url, {headers}).pipe(take(1)).pipe(map(response => Object.assign({
      status: 200,
      data: response
    })), catchError(err => throwError({status: err.status, error: err})))
  }

  public findMovieRecommendations(movieId: number, page: number,  language = 'en-US'): Observable<{
    status: number,
    data: IMoviesData
  }> {

    const headers = this.headers
    const url = `${environmentApi.BACKEND_URL}/movie/${movieId}/recommendations?language=en-US&page=${page}`;
    return this.http.get<ITLMovieInfo>(url, {headers}).pipe(take(1)).pipe(map(response => Object.assign({
      status: 200,
      data: response
    })), catchError(err => throwError({status: err.status, error: err})))
  }
}
