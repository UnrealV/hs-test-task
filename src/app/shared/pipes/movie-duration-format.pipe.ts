import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'movieDurationFormat'
})
export class MovieDurationFormatPipe implements PipeTransform {

  transform(value: number, ...args: unknown[]): unknown {
    return Math.floor(value / 60) + 'h ' + value % 60 + 'm'  ;
  }

}
