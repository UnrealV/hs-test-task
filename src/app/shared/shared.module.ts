import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SearchBarComponent} from "./components/search-bar/search-bar.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ImdbRatingComponent} from './components/imdbrating/imdb-rating.component';
import {IsMovieAdultBadgeComponent} from './components/is-movie-adult-badge/is-movie-adult-badge.component';
import {MovieDurationFormatPipe} from './pipes/movie-duration-format.pipe';
import {MovieComponent} from "./components/movie/movie.component";
import {MovieListComponent} from "./components/movie-list/movie-list.component";
import {PaginationComponent} from "./components/pagination/pagination.component";


@NgModule({
  declarations: [SearchBarComponent, ImdbRatingComponent, IsMovieAdultBadgeComponent, MovieDurationFormatPipe, MovieComponent, MovieListComponent, PaginationComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [SearchBarComponent, ImdbRatingComponent, IsMovieAdultBadgeComponent, MovieDurationFormatPipe, MovieComponent, MovieListComponent, PaginationComponent]
})
export class SharedModule {
}
