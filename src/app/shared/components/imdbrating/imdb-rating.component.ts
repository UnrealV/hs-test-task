import {Component, Input} from '@angular/core';
@Component({
  selector: 'app-imdb-rating',
  templateUrl: './imdb-rating.component.html',
  styleUrls: ['./imdb-rating.component.scss']
})
export class ImdbRatingComponent {
 @Input() voteAverage: number = 0
 @Input() backGroundDark: boolean = false
}
