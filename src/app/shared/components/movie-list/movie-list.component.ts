import {Component, EventEmitter, Input, Output} from '@angular/core';
import {IMoviesData} from "../../interfaces/movies-data.interface";

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss']
})
export class MovieListComponent {
  constructor() {
  }
  @Output() returnMovieId:EventEmitter<number> = new EventEmitter<number>
  @Input() moviesData: IMoviesData

  public navigateToMovieInfo(movieId: number): void {
    this.returnMovieId.emit(movieId)
  }
}
