import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-is-movie-adult-badge',
  templateUrl: './is-movie-adult-badge.component.html',
  styleUrls: ['./is-movie-adult-badge.component.scss']
})
export class IsMovieAdultBadgeComponent {
  @Input() isAdult: boolean = false
}
