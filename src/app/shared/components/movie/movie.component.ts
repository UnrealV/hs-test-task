import {Component, Input} from '@angular/core';
import {IMovieData} from "../../interfaces/movie-data.interface";
import {environmentApi} from "../../../../environment/environment.api";

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss']
})
export class MovieComponent {
  @Input() movieData: IMovieData
  protected readonly environmentApi = environmentApi;
}
