import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent {
  @Input() selectedPage: number = 1
  @Input() totalPages: number = 1
  @Output() pageMovementDirection: EventEmitter< 'back' | 'forward'> = new EventEmitter<"back" | "forward">()
  public moveToPage(direction: 'back' | 'forward'): void {
    this.pageMovementDirection.emit(direction)
  }
  public isBtnDisabled(direction: 'back' | 'forward'): boolean {
    if (direction === 'back') {
      return this.selectedPage == 1
    } else if (direction === 'forward'){
      return this.selectedPage == this.totalPages
    } else return true
  }
}
