import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {MovieService} from "../../services/movie.service";
import {IMovieData} from "../../interfaces/movie-data.interface";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ReplaySubject, takeUntil} from "rxjs";

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit, OnDestroy {
  constructor(
    private movieServiceService: MovieService,
  ) {
  }

  @Output() returnMovieId:EventEmitter<number> = new EventEmitter<number>
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  searchForm = new FormGroup({
    searchBar: new FormControl(''),
  })

  private get getSearchBarInput(): FormControl<any> {
    return this.searchForm.controls.searchBar
  }

  private get getSearchBarInputValue(): string {
    return this.searchForm.controls.searchBar.value + ''
  }

  public navigateToMovieInfo(movieId: number): void {
    this.returnMovieId.emit(movieId)
  }

  public dropdownMovieList: IMovieData[] = []

  searchMovies() {
    if (this.getSearchBarInputValue.trim() !== '') {
      this.movieServiceService.findMovieByName(this.getSearchBarInputValue).subscribe(movieData => {
        this.dropdownMovieList = movieData.data.results.sort((a, b) => b.vote_average - a.vote_average)
      })
    }
  }

  public clearSearchBar(): void {
    this.getSearchBarInput.setValue('')
    this.dropdownMovieList = []
  }

  public ngOnInit(): void {
    this.getSearchBarInput.valueChanges.pipe(takeUntil(this.destroyed$)).subscribe(event => {
      if (event.length === 0) {
        this.dropdownMovieList = []
      }
    })
  }

  public ngOnDestroy(): void {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }
}
