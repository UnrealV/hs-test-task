import {IMovieData} from "./movie-data.interface";

export interface IMoviesData {
  page: number
  results: IMovieData[]
  total_pages: number
  total_results: number
}
