import {IMovieSpokenLanguage} from "./movie-spoken-languages.interface";
import {IProductionCountry} from "./production-country.interface";
import {IMovieGenre} from "./movie-genre.interface";
import {IProductionCompany} from "./production-company.interface";

export interface ITLMovieInfo {
  adult: boolean
  backdrop_path: string
  belongs_to_collection: any
  budget: number
  genres: IMovieGenre[]
  homepage: string
  id: number
  imdb_id: string
  original_language: string
  original_title: string
  overview: string
  popularity: number
  poster_path: string
  production_companies: IProductionCompany[]
  production_countries: IProductionCountry[]
  release_date: string
  revenue: number
  runtime: number
  spoken_languages: IMovieSpokenLanguage[]
  status: string
  tagline: string
  title: string
  video: boolean
  vote_average: number
  vote_count: number
}





