import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {AdminModule} from "./admin/admin.module";
import {MovieService} from "./shared/services/movie.service";
import {HttpClientModule} from "@angular/common/http";
import {SearchBarComponent} from './shared/components/search-bar/search-bar.component';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
  ],
  providers: [
    MovieService
  ],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
