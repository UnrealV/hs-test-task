import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdminRoutingModule} from "./admin-routing.module";
import {HomePageComponent} from './pages/home-page/home-page.component';
import {SharedModule} from "../shared/shared.module";
import {MovieInfoComponent} from "./pages/movie-info/movie-info.component";

@NgModule({
  declarations: [
    HomePageComponent,
    MovieInfoComponent
  ],
  exports: [],
  imports: [
    CommonModule,
    AdminRoutingModule,
    SharedModule
  ]
})
export class AdminModule {
}
