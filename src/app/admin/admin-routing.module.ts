import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomePageComponent} from "./pages/home-page/home-page.component";
import {MovieInfoComponent} from "./pages/movie-info/movie-info.component";

const routes: Routes = [
  {path: 'home', component: HomePageComponent},
  {path: 'movie-info', component: MovieInfoComponent},
  {path: '', redirectTo: 'home', pathMatch: 'full'}
];

export const AdminRoutingModule: ModuleWithProviders<any> = RouterModule.forChild(routes);
