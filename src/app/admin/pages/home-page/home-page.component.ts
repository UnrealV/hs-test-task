import {Component, OnDestroy, OnInit} from '@angular/core';
import {MovieService} from "../../../shared/services/movie.service";
import * as moment from "moment";
import {IMovieData} from "../../../shared/interfaces/movie-data.interface";
import {ActivatedRoute, Router} from "@angular/router";
import {ReplaySubject, take, takeUntil} from "rxjs";
import {IMoviesData} from "../../../shared/interfaces/movies-data.interface";

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit, OnDestroy {
  constructor(
    private movieServiceService: MovieService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
  }
  public selectedPage: number = 1
  public movieList: IMoviesData

  public ngOnInit(): void {
    this.getPopularMovieList()
  }

  private getPopularMovieList(): void {
    this.movieServiceService.getPopularMoviesByPage(this.selectedPage).pipe(take(1)).subscribe(movies => {
      this.movieList = movies.data
      this.movieList.results.sort((a, b) => b.vote_average - a.vote_average)
    })
  }

  public navigateToMovieInfo(movieId: number): void {
    this.router.navigate(['../movie-info'], {relativeTo: this.activatedRoute, queryParams: {movieId: movieId, page: 1}})
  }

  public moveToPage(direction: 'back' | 'forward'): void {
    direction === 'forward' ? ++this.selectedPage : direction === 'back' && this.selectedPage > 1 ? this.selectedPage-- : this.selectedPage
    this.getPopularMovieList()
  }
  public ngOnDestroy(): void {
  }
}
