import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ReplaySubject, take, takeUntil} from "rxjs";
import {MovieService} from "../../../shared/services/movie.service";
import {ITLMovieInfo} from "../../../shared/interfaces/tl-movie-info.interface";
import {IMoviesData} from "../../../shared/interfaces/movies-data.interface";
import {environmentApi} from "../../../../environment/environment.api";

@Component({
  selector: 'app-movie-info',
  templateUrl: './movie-info.component.html',
  styleUrls: ['./movie-info.component.scss']
})
export class MovieInfoComponent implements OnInit, OnDestroy {
  constructor(
    private route: ActivatedRoute,
    private movieServiceService: MovieService,
    private router: Router,
  ) {
  }

  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  public movieInfo: ITLMovieInfo
  public movieRecommendations: IMoviesData
  public selectedPage: number = 1

  public navigateToMovieInfo(movieId: number, page: number = 1): void {
    this.selectedPage = page
    this.router.navigate([], {queryParams: {movieId: movieId, page: page}})
  }

  ngOnInit() {
    this.getMovieData()
  }

  private getMovieData(): void {
    this.route.queryParams.pipe(takeUntil(this.destroyed$)).subscribe(params => {
      const movieId: number = params['movieId']
      this.selectedPage = params['page'] ? params['page'] : 1
      this.movieServiceService.getMovieDetailsById(movieId + '').pipe(take(1)).subscribe(movieInfo => {
        this.movieInfo = movieInfo.data
        this.movieServiceService.findMovieRecommendations(this.movieInfo.id, this.selectedPage).pipe(take(1)).subscribe(recommendations => {
          this.movieRecommendations = recommendations.data
          this.movieRecommendations.results.sort((a, b) => b.vote_average - a.vote_average)
        })
      })
    })
  }

  public moveToPage(direction: 'back' | 'forward'): void {
    direction === 'forward' ? ++this.selectedPage : direction === 'back' && this.selectedPage > 1 ? this.selectedPage-- : this.selectedPage
    this.navigateToMovieInfo(this.movieInfo.id, this.selectedPage)
  }

  ngOnDestroy() {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }

  protected readonly environmentApi = environmentApi;
}
